import requests
import setting
from flask import Flask, request
import time


class Reddit(object):
    AUTH_URL = "https://www.reddit.com/api/v1/authorize"
    TOKEN_URL = "https://www.reddit.com/api/v1/access_token"
    API_URL = "https://oauth.reddit.com/"

    def __init__(self,
                 client_id,
                 client_secret,
                 redirect_uri,
                 user_agent="Simple OAuth/Reddit API wrapper 0.1.0 by u/darkzuik"):
        self.client_id = client_id
        self.client_secret = client_secret
        self.redirect_uri = redirect_uri
        self.user_agent = user_agent

    def build_url(self, scope):
        return self.AUTH_URL + "?client_id={client_id}&response_type={type}&state={state}&redirect_uri={redirect_uri}&duration={duration}&scope={scope}".format(
            state="cats",
            duration="permanent",
            redirect_uri=self.redirect_uri,
            type="code",
            scope=" ".join(scope),
            client_id=self.client_id)

    def get_tokens(self, code=None):
        if not code:
            code = self.code
        r = requests.post(self.TOKEN_URL,
                          auth=(self.client_id, self.client_secret),
                          data={"grant_type": "authorization_code",
                                "code": code,
                                "redirect_uri": self.redirect_uri},
                          headers={"User-Agent": self.user_agent})
        if r.status_code == 200:
            self.access_token = r.json()['access_token']
            self.refresh_token = r.json()['refresh_token']
            self.grant_time = time.time()
            self.expires = time.time() + r.json()['expires_in']
        return r

    def refresh_token(self):
        if time.time() < self.expires:
            print("Not expired yet")
        else:
            r = requests.post(self.TOKEN_URL,
                              data={"grant_type": "refresh_token",
                                    "refresh_token": self.refresh_token},
                              headers={"User-Agent": self.user_agent},
                              auth=(self.client_id, self.client_secret))
            if r.status_code == 200:
                self.access_token = r.json()['access_token']
                self.grant_time = time.time()
                self.expires = time.time() + r.json('expires_in')
            return r

    def call_api(self, end_point):
        headers = {
            "Authorization": "bearer " + self.access_token,
            "User-Agent": self.user_agent
        }
        r = requests.get(self.API_URL + end_point, headers=headers)
        return r
