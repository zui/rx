import requests
import os
import sys
import time

def download(url, filename = None, path=os.getcwd()):
    filename = filename or url.split('/')[-1]
    path = os.path.join(path, filename)
    r = requests.get(url, stream=True)
    with open(path, "wb") as f:
        #print_ctr = 1
        start_time = time.time()
        for i in r.iter_content(chunk_size=1024):
            if i:
                f.write(i)
                #print("Wrote chunk: "+ str(print_ctr))
                #print_ctr += 1
                print_pg_bar(start_time, time.time(), int(r.headers['Content-Length']), f.tell())
                f.flush()
    return path

def print_pg_bar(start_time, current_time, content_length, current_length):
    x = int(current_length/content_length*100)
    download_speed = (current_length/1024)/(current_time-start_time)
    text = "|{0}{1}{2}|| {3}% | {4:.2f}kb/s | t={5:.2f} | byte {6} of {7}".format(
        "="*int(round(x/100*30)),
        "<3",
        "="*(30-int(round(x/100*30))),
        str(x),
        download_speed,
        current_time-start_time, current_length, content_length)
    #while len(text) < int(columns):
    #    text += " "
    sys.stdout.write("\b"*(len(text)+1))
    sys.stdout.write(text)
    sys.stdout.flush()
    if x == 100:
        sys.stdout.write("\n")