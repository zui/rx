import requests
from bs4 import BeautifulSoup
import re
import os
import time
from pymongo import MongoClient
from celery import Celery

client = MongoClient()
db = client.russia.test
headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.37 Safari/537.36"}
app = Celery('russia', broker='mongodb://localhost:27017/celery')

@app.task
def download_image(url=None, thread_id=None, post_id=None, path=os.getcwd(), order=None, download=True):
    start_time = time.time()
    filename = url.split("/")[-1]
    folder = os.path.join(path, thread_id)
    path = os.path.join(path, thread_id, "[{}]{}_{}".format(post_id, order, filename))
    if not os.path.exists(folder):
        os.makedirs(folder)
    if download:
        with open(path, 'wb') as f:
            r = requests.get(url, stream=True)
            for i in r.iter_content(chunk_size=1024):
                if i:
                    f.write(i)
                    f.flush()
        print('Downloaded: {} in {:.2}s'.format(path, (time.time()-start_time)))
    return path
def get_threads(page_url = None, forum_id=28, page_number=1, objected=True):
    if not page_url:
        page_url = 'http://thiendia.com/diendan/forumdisplay.php?s=&f={}&page={}&pp=22&sort=dateline&order=desc&daysprune=-1'.format(
        forum_id, page_number)
    r = requests.get(page_url, headers=headers)
    b = BeautifulSoup(r.text, 'html.parser')
    b = b.find(id='threadslist')
    b = b.find_all('a', id=re.compile('^thread_title_\d+'))
    t = []
    if objected:
        for x in b:
            url = "http://thiendia.com/diendan/"+x['href']
            print("Getting:{}".format(url))
            t.append(ForumThread(url=url))
        return t
    return [x for x in b]

class ForumThread(object):
    """
    This class represent a thread page. Use download will get all image in the post that has images.
    :var title:
    :var _id:
    :var posts: A list of posts contain in the **first** page of the thread
    :var image_posts: A list of posts containing images. Only these will be downloaded
    """
    def __init__(self, url=None, b=None):
        """
        :param url: Thread url as str
        """
        if not b:
            r = requests.get(url, headers=headers)
            b = BeautifulSoup(r.text, 'html.parser')
        self.url = url
        self.title = self.get_title(b)
        self._id = re.search('\d+', url).group(0)
        self.posts = self.get_posts(b)
        self.image_posts = self.get_images_post()
    def get_title(self, thread_text):
        try:
            title = thread_text.find('a', attrs={"href":self.url}).find('h1').text
        except AttributeError:
            title = ""
        return title
    def get_posts(self, thread_text):
        b = thread_text.find('div', id="posts")
        posts = re.findall('<!-- post #(?P<post_id>\d+) -->', str(b))
        r = []
        for post_id in posts:
            post_text = b.find('table', id='post{}'.format(post_id))
            r.append(PostMessage(post_id, post_text))
        return r
    def get_images_post(self):
        image_posts = [x for x in self.posts if x.has_image]
        return image_posts
    def download(self, download=True):
        """
        Notice this will return a list inside a list. 
        Sometime, people will post images on multiple post, 
        thus every element of the outter list will be a post.
        """
        r = []
        for x in self.image_posts:
            r.append(x.download(self._id, download=download))
        return r
    def to_db(self):
        return {
            "title":self.title,
            "_id":"t_"+self._id,
            "posts":[x.to_db() for x in self.posts],
            "image_posts":[y._id for y in self.image_posts],
            "url":self.url,
        }
    def insert_to_db(self, db_driver = db):
        if not db_driver.find_one("t_"+self._id): 
            self.download()
            db_driver.insert_one(self.to_db())
        else:
            return None
class PostMessage(object):
    """
    A single post from a thread.
    :var _id:
    :var text: BeautifulSoup object of the body of the post
    :var images: List of images in the post
    :var has_image:
    :var author: A dict of username and id of the author of the post
    :var time: Date and time string in the post, GMT +7
    """
    def __init__(self, post_id, post_text):
        self._id = post_id
        self.text = post_text
        self.images = self.get_image()
        if not self.images:
            self.has_image = False
        else:
            self.has_image = True
        self.author = self.get_author()
        self.time = self.get_time()
        self.image_paths = []
    def download(self, thread_id, download=True):
        self.image_paths = []
        for order, url in enumerate(self.images):
            #self.image_paths.append(download_image(url=url,
             #                                      thread_id=thread_id,
              #                                     post_id=self._id,
               #                                    order=order+1,
                #                                   download=download))
            download_image.delay(url=url, thread_id=thread_id, post_id=self._id, order=order+1, download=download)
        return self.image_paths
    def get_time(self):
        try:
            return re.search('(\d{2})-(\d{2})-(\d{4}), (\d{2}):(\d{2}) (AM|PM)', str(self.text)).group(0)
        except AttributeError:
            """
            Things like "Hôm nay" cause problem for the regex.
            """
            return None
    def get_image(self):
        """
        :returns: A list of image urls
        """
        post_text = self.text.find('div', id='post_message_{}'.format(self._id))
        self.post_body = post_text
        images = []
        if not post_text:
            return []
        for x in post_text.find_all('img'):
            if x.has_attr('class') and 'inlineimg' in x['class']: #BeautifulSoup's class return a list
                pass
            else:
                images.append(x['src'])
        return images
    def get_author(self):
        text = self.text.find('div', id='postmenu_{}'.format(self._id))
        try:
            username = text.find('a', class_="bigusername").text
            user_id = re.search('\?u\=(?P<id>\d+)', text.find('a')['href']).group('id')
        except AttributeError:
            print(self._id)
            username = ""
            user_id = "00"
        return {
            "username":username,
            "user_id":"u_"+user_id,
        }
    def to_db(self):
        return {
            "post_id":"p_"+self._id,
            "post_author":self.author,
            "post_body":str(self.post_body),
            "image_urls":self.images,
            "image_paths":self.image_paths,            
        }
def insert_to_db(url, db_driver=db):
    _id = re.search('\d+', url).group(0)
    if not db_driver.find_one("t_"+_id): 
        a = ForumThread(url=url)
        a.download()
        db_driver.insert_one(a.to_db())
    else:
        return None
